﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Parameters;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Services.OngoingCourses
{
    public class GetOngoingCourseServiceDecorator : IService<GetOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IService<GetOngoingCourseParameter, OngoingCourseDto> _getOngoingCourseService;
        private readonly ILogger<GetOngoingCourseServiceDecorator> _logger;

        public GetOngoingCourseServiceDecorator(
            IService<GetOngoingCourseParameter, OngoingCourseDto> getOngoingCourseService,
            ILogger<GetOngoingCourseServiceDecorator> logger)
        {
            _getOngoingCourseService = getOngoingCourseService;
            _logger = logger;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(GetOngoingCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _getOngoingCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<OngoingCourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
