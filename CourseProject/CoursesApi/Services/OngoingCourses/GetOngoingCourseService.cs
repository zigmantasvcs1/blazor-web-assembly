﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Courses;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Services.OngoingCourses
{
    public class GetOngoingCourseService : IService<GetOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IRepository<OngoingCourse> _ongoingCourseRepository;

        public GetOngoingCourseService(IRepository<OngoingCourse> ongoingCourseRepository)
        {
            _ongoingCourseRepository = ongoingCourseRepository;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(GetOngoingCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _ongoingCourseRepository.GetAsync(parameter.Id);

            var ongoingCourseDto = Convert(result);

            return new Result<OngoingCourseDto>(200, ongoingCourseDto);
        }

        private OngoingCourseDto Convert(OngoingCourse ongoingCourse)
        {
            return new OngoingCourseDto()
            {
                Id = ongoingCourse.Id,
                StartDate = ongoingCourse.StartDate,
                EndDate = ongoingCourse.EndDate,
                Course = new CourseDto()
                {
                    Id = ongoingCourse.Course.Id,
                    Price = ongoingCourse.Course.Price,
                    Title = ongoingCourse.Course.Title,
                    Hours = ongoingCourse.Course.Hours,
                    Description = ongoingCourse.Course.Description
                }
            };
        }
    }
}
