﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Parameters;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Services.OngoingCourses
{
    public class UpdateOngoingCourseServiceDecorator : IService<UpdateOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IService<UpdateOngoingCourseParameter, OngoingCourseDto> _updateOngoingCourseService;
        private readonly ILogger<UpdateOngoingCourseServiceDecorator> _logger;

        public UpdateOngoingCourseServiceDecorator(
            IService<UpdateOngoingCourseParameter, OngoingCourseDto> updateOngoingCourseService,
            ILogger<UpdateOngoingCourseServiceDecorator> logger)
        {
            _updateOngoingCourseService = updateOngoingCourseService;
            _logger = logger;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(UpdateOngoingCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _updateOngoingCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<OngoingCourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
