﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Parameters;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Services.OngoingCourses
{
    public class DeleteOngoingCourseServiceDecorator : IService<DeleteOngoingCourseParameter, OngoingCourseDto>
    {
        private readonly IService<DeleteOngoingCourseParameter, OngoingCourseDto> _deleteOngoingCourseService;
        private readonly ILogger<DeleteOngoingCourseServiceDecorator> _logger;

        public DeleteOngoingCourseServiceDecorator(
            IService<DeleteOngoingCourseParameter, OngoingCourseDto> deleteOngoingCourseService,
            ILogger<DeleteOngoingCourseServiceDecorator> logger)
        {
            _deleteOngoingCourseService = deleteOngoingCourseService;
            _logger = logger;
        }

        public async Task<Result<OngoingCourseDto>> CallAsync(DeleteOngoingCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _deleteOngoingCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<OngoingCourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
