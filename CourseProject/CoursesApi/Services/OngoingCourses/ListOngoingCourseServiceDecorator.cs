﻿using CoursesApi.Models;
using CoursesApi.Models.OngoingCourses.Parameters;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Services.OngoingCourses
{
    public class ListOngoingCourseServiceDecorator : IService<ListOngoingCourseParameter, List<OngoingCourseDto>>
    {
        private readonly IService<ListOngoingCourseParameter, List<OngoingCourseDto>> _listOngoingCourseService;
        private readonly ILogger<ListOngoingCourseServiceDecorator> _logger;

        public ListOngoingCourseServiceDecorator(
            IService<ListOngoingCourseParameter, List<OngoingCourseDto>> listOngoingCourseService,
            ILogger<ListOngoingCourseServiceDecorator> logger)
        {
            _listOngoingCourseService = listOngoingCourseService;
            _logger = logger;
        }

        public async Task<Result<List<OngoingCourseDto>>> CallAsync(ListOngoingCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _listOngoingCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<List<OngoingCourseDto>>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
