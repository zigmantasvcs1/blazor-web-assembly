﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Parameters;
using Shared.Dto.Courses;

namespace CoursesApi.Services.Courses
{
    public class ListCourseServiceDecorator : IService<ListCourseParameter, List<CourseDto>>
    {
        private readonly IService<ListCourseParameter, List<CourseDto>> _listCourseService;
        private readonly ILogger<ListCourseServiceDecorator> _logger;

        public ListCourseServiceDecorator(
            IService<ListCourseParameter, List<CourseDto>> listCourseService,
            ILogger<ListCourseServiceDecorator> logger)
        {
            _listCourseService = listCourseService;
            _logger = logger;
        }

        public async Task<Result<List<CourseDto>>> CallAsync(ListCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _listCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<List<CourseDto>>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
