﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Parameters;
using Shared.Dto.Courses;

namespace CoursesApi.Services.Courses
{
    public class DeleteCourseServiceDecorator : IService<DeleteCourseParameter, CourseDto>
    {
        private readonly IService<DeleteCourseParameter, CourseDto> _deleteCourseService;
        private readonly ILogger<DeleteCourseServiceDecorator> _logger;

        public DeleteCourseServiceDecorator(
            IService<DeleteCourseParameter, CourseDto> deleteCourseService,
            ILogger<DeleteCourseServiceDecorator> logger)
        {
            _deleteCourseService = deleteCourseService;
            _logger = logger;
        }

        public async Task<Result<CourseDto>> CallAsync(DeleteCourseParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _deleteCourseService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<CourseDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
