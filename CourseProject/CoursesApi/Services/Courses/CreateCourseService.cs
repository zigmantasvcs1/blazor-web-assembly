﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Providers;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Courses;

namespace CoursesApi.Services.Courses
{
    public class CreateCourseService : IService<CreateCourseParameter, CourseDto>
    {
        private readonly IRepository<Course> _courseRepository;

        private readonly IDateTimeNowProvider _dateTimeNowProvider;

        public CreateCourseService(
            IRepository<Course> courseRepository,
            IDateTimeNowProvider dateTimeNowProvider)
        {
            _courseRepository = courseRepository;
            _dateTimeNowProvider = dateTimeNowProvider;
        }

        public async Task<Result<CourseDto>> CallAsync(CreateCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            // konvertuojame is Course dto i entity
            var courseEntity = Convert(parameter.Course);

            var result = await _courseRepository.CreateAsync(courseEntity);

            // gauta entity konvertuojame i dto
            var courseDto = Convert(result);

            return new Result<CourseDto>(200, courseDto);
        }

        private Course Convert(CreateCourseDto courseDto)
        {
            return new Course()
            {
                Price = courseDto.Price,
                Title = courseDto.Title,
                Hours = courseDto.Hours,
                Description = courseDto.Description,
                CreatedAt = _dateTimeNowProvider.Get()
            };
        }

        private CourseDto Convert(Course course)
        {
            return new CourseDto()
            {
                Id = course.Id,
                Price = course.Price,
                Title = course.Title,
                Hours = course.Hours,
                Description = course.Description
            };
        }
    }
}
