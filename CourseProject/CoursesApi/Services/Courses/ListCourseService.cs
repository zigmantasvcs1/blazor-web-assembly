﻿using CoursesApi.Models;
using CoursesApi.Models.Courses.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Courses;

namespace CoursesApi.Services.Courses
{
    public class ListCourseService : IService<ListCourseParameter, List<CourseDto>>
    {
        private readonly IRepository<Course> _coursesRepository;

        public ListCourseService(IRepository<Course> coursesRepository)
        {
            _coursesRepository = coursesRepository;
        }

        public async Task<Result<List<CourseDto>>> CallAsync(ListCourseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _coursesRepository.ListAsync(parameter.Limit);

            var courseDtos = result
                .Select(Convert)
                .ToList();

            return new Result<List<CourseDto>>(200, courseDtos);
        }

        private CourseDto Convert(Course course)
        {
            return new CourseDto()
            {
                Id = course.Id,
                Price = course.Price,
                Title = course.Title,
                Hours = course.Hours,
                Description = course.Description
            };
        }
    }
}
