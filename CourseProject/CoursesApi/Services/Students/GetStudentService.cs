﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class GetStudentService : IService<GetStudentParameter, StudentDto>
    {
        private readonly IRepository<Student> _studentRepository;

        public GetStudentService(IRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<Result<StudentDto>> CallAsync(GetStudentParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _studentRepository.GetAsync(parameter.Id);

            var studentDto = Convert(result);

            return new Result<StudentDto>(200, studentDto);
        }

        private StudentDto Convert(Student student)
        {
            return new StudentDto()
            {
                Id = student.Id,
                Name = student.Name,
                Surname = student.Surname,
                Email = student.Email,
                BirthDay = student.BirthDay,
                DocumentNumber = student.DocumentNumber
            };
        }
    }
}
