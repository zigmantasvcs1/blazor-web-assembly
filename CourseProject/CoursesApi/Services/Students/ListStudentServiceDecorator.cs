﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class ListStudentServiceDecorator : IService<ListStudentParameter, List<StudentDto>>
    {
        private readonly IService<ListStudentParameter, List<StudentDto>> _listStudentService;
        private readonly ILogger<ListStudentServiceDecorator> _logger;

        public ListStudentServiceDecorator(
            IService<ListStudentParameter, List<StudentDto>> listStudentService,
            ILogger<ListStudentServiceDecorator> logger)
        {
            _listStudentService = listStudentService;
            _logger = logger;
        }

        public async Task<Result<List<StudentDto>>> CallAsync(ListStudentParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _listStudentService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<List<StudentDto>>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
