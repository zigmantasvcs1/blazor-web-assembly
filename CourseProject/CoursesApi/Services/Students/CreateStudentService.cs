﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using CoursesApi.Providers;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class CreateStudentService : IService<CreateStudentParameter, StudentDto>
    {
        private readonly IRepository<Student> _studentRepository;

        private readonly IDateTimeNowProvider _dateTimeNowProvider;

        public CreateStudentService(
            IRepository<Student> studentRepository,
            IDateTimeNowProvider dateTimeNowProvider)
        {
            _studentRepository = studentRepository;
            _dateTimeNowProvider = dateTimeNowProvider;
        }

        public async Task<Result<StudentDto>> CallAsync(CreateStudentParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            // konvertuojame is Student dto i entity
            var studentEntity = Convert(parameter.Student);

            var result = await _studentRepository.CreateAsync(studentEntity);

            // gauta entity konvertuojame i dto
            var studentDto = Convert(result);

            return new Result<StudentDto>(200, studentDto);
        }

        private Student Convert(CreateStudentDto studentDto)
        {
            return new Student()
            {
                Name = studentDto.Name,
                Surname = studentDto.Surname,
                Email = studentDto.Email,
                BirthDay = studentDto.BirthDay,
                DocumentNumber = studentDto.DocumentNumber,
                CreatedAt = _dateTimeNowProvider.Get()
            };
        }

        private StudentDto Convert(Student student)
        {
            return new StudentDto()
            {
                Id = student.Id,
                Name = student.Name,
                Surname = student.Surname,
                Email = student.Email,
                BirthDay = student.BirthDay,
                DocumentNumber = student.DocumentNumber
            };
        }
    }
}
