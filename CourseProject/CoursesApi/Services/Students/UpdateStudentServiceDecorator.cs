﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class UpdateStudentServiceDecorator : IService<UpdateStudentParameter, StudentDto>
    {
        private readonly IService<UpdateStudentParameter, StudentDto> _updateStudentService;
        private readonly ILogger<UpdateStudentServiceDecorator> _logger;

        public UpdateStudentServiceDecorator(
            IService<UpdateStudentParameter, StudentDto> updateStudentService,
            ILogger<UpdateStudentServiceDecorator> logger)
        {
            _updateStudentService = updateStudentService;
            _logger = logger;
        }

        public async Task<Result<StudentDto>> CallAsync(UpdateStudentParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _updateStudentService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<StudentDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
