﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class UpdateStudentService : IService<UpdateStudentParameter, StudentDto>
    {
        private readonly IRepository<Student> _studentRepository;

        public UpdateStudentService(IRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<Result<StudentDto>> CallAsync(UpdateStudentParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var studentToUpdate = await _studentRepository.GetAsync(parameter.Student.Id);

            var studentEntity = Convert(parameter.Student, studentToUpdate);

            var result = await _studentRepository.UpdateAsync(studentEntity);

            var studentDto = Convert(result);

            return new Result<StudentDto>(200, studentDto);
        }

        private Student Convert(UpdateStudentDto studentDto, Student studentEntity)
        {
            studentEntity.Name = studentDto.Name;
            studentEntity.Surname = studentDto.Surname;
            studentEntity.Email = studentDto.Email;
            studentEntity.BirthDay = studentDto.BirthDay;
            studentEntity.DocumentNumber = studentDto.DocumentNumber;

            return studentEntity;

            //return new Student()
            //{
            //    Id = studentDto.Id,
            //    Name = studentDto.Name,
            //    Surname = studentDto.Surname,
            //    Email = studentDto.Email,
            //    BirthDay = studentDto.BirthDay,
            //    DocumentNumber = studentDto.DocumentNumber
            //};
        }

        private StudentDto Convert(Student student)
        {
            return new StudentDto()
            {
                Id = student.Id,
                Name = student.Name,
                Surname = student.Surname,
                Email = student.Email,
                BirthDay = student.BirthDay,
                DocumentNumber = student.DocumentNumber
            };
        }
    }
}
