﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class DeleteStudentService : IService<DeleteStudentParameter, StudentDto>
    {
        private readonly IRepository<Student> _studentRepository;

        public DeleteStudentService(IRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<Result<StudentDto>> CallAsync(DeleteStudentParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _studentRepository.DeleteAsync(parameter.Id);

            if (result)
            {
                return new Result<StudentDto>(200, new StudentDto());
            }

            return new Result<StudentDto>(404, null);
        }
    }
}
