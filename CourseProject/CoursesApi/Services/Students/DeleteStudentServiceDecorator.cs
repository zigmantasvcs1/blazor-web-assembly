﻿using CoursesApi.Models;
using CoursesApi.Models.Students.Parameters;
using Shared.Dto.Students;

namespace CoursesApi.Services.Students
{
    public class DeleteStudentServiceDecorator : IService<DeleteStudentParameter, StudentDto>
    {
        private readonly IService<DeleteStudentParameter, StudentDto> _deleteStudentService;
        private readonly ILogger<DeleteStudentServiceDecorator> _logger;

        public DeleteStudentServiceDecorator(
            IService<DeleteStudentParameter, StudentDto> deleteStudentService,
            ILogger<DeleteStudentServiceDecorator> logger)
        {
            _deleteStudentService = deleteStudentService;
            _logger = logger;
        }

        public async Task<Result<StudentDto>> CallAsync(DeleteStudentParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _deleteStudentService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<StudentDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
