﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class ListLecturerServiceDecorator : IService<ListLecturerParameter, List<LecturerDto>>
    {
        private readonly IService<ListLecturerParameter, List<LecturerDto>> _listLecturerService;
        private readonly ILogger<ListLecturerServiceDecorator> _logger;

        public ListLecturerServiceDecorator(
            IService<ListLecturerParameter, List<LecturerDto>> listLecturerService,
            ILogger<ListLecturerServiceDecorator> logger)
        {
            _listLecturerService = listLecturerService;
            _logger = logger;
        }

        public async Task<Result<List<LecturerDto>>> CallAsync(ListLecturerParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _listLecturerService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<List<LecturerDto>>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
