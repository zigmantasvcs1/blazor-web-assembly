﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class UpdateLecturerService : IService<UpdateLecturerParameter, LecturerDto>
    {
        private readonly IRepository<Lecturer> _lecturerRepository;

        public UpdateLecturerService(IRepository<Lecturer> lecturerRepository)
        {
            _lecturerRepository = lecturerRepository;
        }

        public async Task<Result<LecturerDto>> CallAsync(UpdateLecturerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var lecturerToUpdate = await _lecturerRepository.GetAsync(parameter.Lecturer.Id);

            var lecturerEntity = Convert(parameter.Lecturer, lecturerToUpdate);

            var result = await _lecturerRepository.UpdateAsync(lecturerEntity);

            var lecturerDto = Convert(result);

            return new Result<LecturerDto>(200, lecturerDto);
        }

        private Lecturer Convert(UpdateLecturerDto lecturerDto, Lecturer lecturerEntity)
        {
            lecturerEntity.Name = lecturerDto.Name;
            lecturerEntity.Surname = lecturerDto.Surname;
            lecturerEntity.Email = lecturerDto.Email;
            lecturerEntity.DocumentNumber = lecturerDto.DocumentNumber;

            return lecturerEntity;
        }

        private LecturerDto Convert(Lecturer lecturer)
        {
            return new LecturerDto()
            {
                Id = lecturer.Id,
                Name = lecturer.Name,
                Surname = lecturer.Surname,
                Email = lecturer.Email,
                DocumentNumber = lecturer.DocumentNumber
            };
        }
    }
}
