﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class CreateLecturerServiceDecorator : IService<CreateLecturerParameter, LecturerDto>
    {
        private readonly IService<CreateLecturerParameter, LecturerDto> _createLecturerService;
        private readonly ILogger<CreateLecturerServiceDecorator> _logger;

        public CreateLecturerServiceDecorator(
            IService<CreateLecturerParameter, LecturerDto> createLecturerService,
            ILogger<CreateLecturerServiceDecorator> logger)
        {
            _createLecturerService = createLecturerService;
            _logger = logger;
        }

        public async Task<Result<LecturerDto>> CallAsync(CreateLecturerParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _createLecturerService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<LecturerDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
