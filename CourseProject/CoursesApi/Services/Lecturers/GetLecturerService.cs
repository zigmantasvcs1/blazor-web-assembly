﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class GetLecturerService : IService<GetLecturerParameter, LecturerDto>
    {
        private readonly IRepository<Lecturer> _lecturerRepository;

        public GetLecturerService(IRepository<Lecturer> lecturerRepository)
        {
            _lecturerRepository = lecturerRepository;
        }

        public async Task<Result<LecturerDto>> CallAsync(GetLecturerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            var result = await _lecturerRepository.GetAsync(parameter.Id);

            var lecturerDto = Convert(result);

            return new Result<LecturerDto>(200, lecturerDto);
        }

        private LecturerDto Convert(Lecturer lecturer)
        {
            return new LecturerDto()
            {
                Id = lecturer.Id,
                Name = lecturer.Name,
                Surname = lecturer.Surname,
                Email = lecturer.Email,
                DocumentNumber = lecturer.DocumentNumber
            };
        }
    }
}
