﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Providers;
using DataAccess;
using DataAccess.Entities;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class CreateLecturerService : IService<CreateLecturerParameter, LecturerDto>
    {
        private readonly IRepository<Lecturer> _lecturerRepository;

        private readonly IDateTimeNowProvider _dateTimeNowProvider;

        public CreateLecturerService(
            IRepository<Lecturer> lecturerRepository,
            IDateTimeNowProvider dateTimeNowProvider)
        {
            _lecturerRepository = lecturerRepository;
            _dateTimeNowProvider = dateTimeNowProvider;
        }

        public async Task<Result<LecturerDto>> CallAsync(CreateLecturerParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            // konvertuojame is Lecturer dto i entity
            var lecturerEntity = Convert(parameter.Lecturer);

            var result = await _lecturerRepository.CreateAsync(lecturerEntity);

            // gauta entity konvertuojame i dto
            var lecturerDto = Convert(result);

            return new Result<LecturerDto>(200, lecturerDto);
        }

        private Lecturer Convert(CreateLecturerDto lecturerDto)
        {
            return new Lecturer()
            {
                Name = lecturerDto.Name,
                Surname = lecturerDto.Surname,
                Email = lecturerDto.Email,
                DocumentNumber = lecturerDto.DocumentNumber,
                CreatedAt = _dateTimeNowProvider.Get()
            };
        }

        private LecturerDto Convert(Lecturer lecturer)
        {
            return new LecturerDto()
            {
                Id = lecturer.Id,
                Name = lecturer.Name,
                Surname = lecturer.Surname,
                Email = lecturer.Email,
                DocumentNumber = lecturer.DocumentNumber
            };
        }
    }
}
