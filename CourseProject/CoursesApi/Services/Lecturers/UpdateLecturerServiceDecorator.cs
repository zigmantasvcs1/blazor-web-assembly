﻿using CoursesApi.Models;
using CoursesApi.Models.Lecturers.Parameters;
using Shared.Dto.Lecturers;

namespace CoursesApi.Services.Lecturers
{
    public class UpdateLecturerServiceDecorator : IService<UpdateLecturerParameter, LecturerDto>
    {
        private readonly IService<UpdateLecturerParameter, LecturerDto> _updateLecturerService;
        private readonly ILogger<UpdateLecturerServiceDecorator> _logger;

        public UpdateLecturerServiceDecorator(
            IService<UpdateLecturerParameter, LecturerDto> updateLecturerService,
            ILogger<UpdateLecturerServiceDecorator> logger)
        {
            _updateLecturerService = updateLecturerService;
            _logger = logger;
        }

        public async Task<Result<LecturerDto>> CallAsync(UpdateLecturerParameter parameter)
        {
            try
            {
                _logger.LogInformation("Calling service with {@Parameter}", parameter);

                return await _updateLecturerService.CallAsync(parameter);
            }
            catch (Exception ex)
            {
                _logger.LogError("Klaida", ex);

                return new Result<LecturerDto>(
                    500,
                    null,
                    new List<string>() { "Kreipkites i adminsitratoriu" }
                );
            }
            finally
            {
                _logger.LogInformation("Calling service ended");
            }
        }
    }
}
