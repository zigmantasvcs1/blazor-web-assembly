﻿using CoursesApi.Models;

namespace CoursesApi.Services
{
    public interface IService<TParameter, TData>
    {
        Task<Result<TData>> CallAsync(TParameter parameter);
    }
}
