﻿using System.Net;
using System.Text;

namespace CoursesApi.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                // galima aprasyti logika kuri patikrina request paimama is context
                if (context.Request.ContentType == "application/json")
                {
                    // Patikrinkite, ar gautas turinys yra validus JSON
                    var isValidJson = await IsValidJson(context.Request);
                    if (!isValidJson)
                    {
                        throw new ClientException("Invalid JSON format.");
                    }
                }

                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";

                switch (error)
                {
                    // cia galima apsirasyti savo exceptionus
                    case ClientException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        break;
                    default:
                        // unhandled error
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }

                var result = System.Text.Json.JsonSerializer.Serialize(new { message = error?.Message });
                await response.WriteAsync(result);
            }
        }

        private async Task<bool> IsValidJson(HttpRequest request)
        {
            try
            {
                // Read the request body to the end
                var requestBody = await new StreamReader(request.Body).ReadToEndAsync();

                // Deserialize the JSON to check if it's valid
                System.Text.Json.JsonSerializer.Deserialize<object>(requestBody);

                // Restore the request body to its original state
                request.Body = new MemoryStream(Encoding.UTF8.GetBytes(requestBody));

                return true;
            }
            catch
            {
                // If deserialization fails, the JSON is not valid
                return false;
            }
        }
    }
}
