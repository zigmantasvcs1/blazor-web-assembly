﻿using Shared.Dto.Students;

namespace CoursesApi.Models.Students.Parameters
{
    public class UpdateStudentParameter
    {
        public UpdateStudentParameter(UpdateStudentDto student)
        {
            Student = student;
        }

        public UpdateStudentDto Student { get; }
    }
}
