﻿namespace CoursesApi.Models.Students.Parameters
{
    public class GetStudentParameter
    {
        public GetStudentParameter(int id)
        {
            if(id < 1)
            {
                throw new ArgumentException("Bad id");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
