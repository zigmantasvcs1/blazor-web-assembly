﻿namespace CoursesApi.Models.Students.Parameters
{
    public class ListStudentParameter
    {
        public ListStudentParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
