﻿namespace CoursesApi.Models.Courses.Parameters
{
    public class GetCourseParameter
    {
        public GetCourseParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Bad id");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
