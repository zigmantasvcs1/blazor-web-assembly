﻿using Shared.Dto.Courses;

namespace CoursesApi.Models.Courses.Parameters
{
    public class CreateCourseParameter
    {
        public CreateCourseParameter(CreateCourseDto course)
        {
            Course = course;
        }

        public CreateCourseDto Course { get; }
    }
}
