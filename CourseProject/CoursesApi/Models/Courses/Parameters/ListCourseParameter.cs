﻿namespace CoursesApi.Models.Courses.Parameters
{
    public class ListCourseParameter
    {
        public ListCourseParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
