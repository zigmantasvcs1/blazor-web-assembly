﻿using Shared.Dto.Courses;

namespace CoursesApi.Models.Courses.Parameters
{
    public class UpdateCourseParameter
    {
        public UpdateCourseParameter(UpdateCourseDto course)
        {
            Course = course;
        }

        public UpdateCourseDto Course { get; }
    }
}
