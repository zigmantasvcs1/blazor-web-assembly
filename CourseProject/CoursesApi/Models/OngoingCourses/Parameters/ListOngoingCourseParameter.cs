﻿namespace CoursesApi.Models.OngoingCourses.Parameters
{
    public class ListOngoingCourseParameter
    {
        public ListOngoingCourseParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
