﻿using Shared.Dto.OngoingCourses;

namespace CoursesApi.Models.OngoingCourses.Parameters
{
    public class CreateOngoingCourseParameter
    {
        public CreateOngoingCourseParameter(CreateOngoingCourseDto ongoingCourse)
        {
            OngoingCourse = ongoingCourse;
        }

        public CreateOngoingCourseDto OngoingCourse { get; }
    }
}
