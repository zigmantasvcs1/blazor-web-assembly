﻿namespace CoursesApi.Models.Lecturers.Parameters
{
    public class GetLecturerParameter
    {
        public GetLecturerParameter(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Bad id");
            }

            Id = id;
        }

        public int Id { get; }
    }
}
