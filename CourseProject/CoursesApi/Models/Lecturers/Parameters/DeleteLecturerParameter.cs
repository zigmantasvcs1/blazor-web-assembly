﻿namespace CoursesApi.Models.Lecturers.Parameters
{
    public class DeleteLecturerParameter
    {
        public DeleteLecturerParameter(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
