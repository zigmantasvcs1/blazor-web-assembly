﻿namespace CoursesApi.Models.Lecturers.Parameters
{
    public class ListLecturerParameter
    {
        public ListLecturerParameter(int? limit)
        {
            Limit = limit;
        }

        public int? Limit { get; }
    }
}
