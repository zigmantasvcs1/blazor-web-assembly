﻿using Shared.Dto.Lecturers;

namespace CoursesApi.Models.Lecturers.Parameters
{
    public class UpdateLecturerParameter
    {
        public UpdateLecturerParameter(UpdateLecturerDto lecturer)
        {
            Lecturer = lecturer;
        }

        public UpdateLecturerDto Lecturer { get; }
    }
}
