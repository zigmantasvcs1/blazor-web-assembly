using CoursesApi.Extensions;
using CoursesApi.Middlewares;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Filters;

namespace CoursesApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

            // turime susieti AppDbContext is DataAcces projekto su connectionStringu
            builder.Services.AddDbContext<CoursesDbContext>(
                options => options
                    .UseLazyLoadingProxies()
                    .UseSqlServer(connectionString)
            );

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Information()
                .WriteTo.File("logs/logs.txt", rollingInterval: RollingInterval.Minute)
                .Filter.ByIncludingOnly(Matching.FromSource("CoursesApi.Services"))
                .CreateLogger();

            builder.Host.UseSerilog();

            // Add services to the container.
            builder.Services.AddApplicationServices();

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin", policy =>
                {
                    policy.WithOrigins("https://localhost:7044")
                          .AllowAnyHeader()
                          .AllowAnyMethod();
                });
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            //if (app.Environment.IsDevelopment())
            //{
                app.UseSwagger();
                app.UseSwaggerUI();
            //}

            app.UseHttpsRedirection();

            app.UseCors("AllowSpecificOrigin");

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
