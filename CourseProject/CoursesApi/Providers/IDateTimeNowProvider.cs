﻿namespace CoursesApi.Providers
{
    public interface IDateTimeNowProvider
    {
        DateTime Get();
    }
}
