﻿namespace CoursesApi.Providers
{
    public class DateTimeNowProvider : IDateTimeNowProvider
    {
        public DateTime Get()
        {
            return DateTime.Now;
        }
    }
}
