﻿using CoursesApi.Models.OngoingCourses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OngoingCoursesController : ControllerBase
    {
        private readonly IService<ListOngoingCourseParameter, List<OngoingCourseDto>> _listOngoingCoursesService;
        private readonly IService<CreateOngoingCourseParameter, OngoingCourseDto> _createOngoingCoursesService;
        private readonly IService<UpdateOngoingCourseParameter, OngoingCourseDto> _updateOngoingCoursesService;
        private readonly IService<GetOngoingCourseParameter, OngoingCourseDto> _getOngoingCoursesService;
        private readonly IService<DeleteOngoingCourseParameter, OngoingCourseDto> _deleteOngoingCoursesService;

        public OngoingCoursesController(
            IService<ListOngoingCourseParameter, List<OngoingCourseDto>> listOngoingCoursesService,
            IService<CreateOngoingCourseParameter, OngoingCourseDto> createOngoingCoursesService,
            IService<UpdateOngoingCourseParameter, OngoingCourseDto> updateOngoingCoursesService,
            IService<GetOngoingCourseParameter, OngoingCourseDto> getOngoingCoursesService,
            IService<DeleteOngoingCourseParameter, OngoingCourseDto> deleteOngoingCoursesService)
        {
            _listOngoingCoursesService = listOngoingCoursesService;
            _createOngoingCoursesService = createOngoingCoursesService;
            _updateOngoingCoursesService = updateOngoingCoursesService;
            _getOngoingCoursesService = getOngoingCoursesService;
            _deleteOngoingCoursesService = deleteOngoingCoursesService;
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync()
        {
            try
            {
                var ongoingCourses = await _listOngoingCoursesService.CallAsync(
                    new ListOngoingCourseParameter(10)
                );

                return Ok(ongoingCourses);
            }
            catch (Exception ex)
            {
                return StatusCode(
                    500,
                    new Result<OngoingCourse>(
                        500,
                        null,
                        new List<string>() { "Klaida" }
                    )
                );
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var result = await _getOngoingCoursesService.CallAsync(new GetOngoingCourseParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<OngoingCourse>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateOngoingCourseDto ongoingCourse)
        {
            var result = await _createOngoingCoursesService.CallAsync(
                new CreateOngoingCourseParameter(ongoingCourse)
            );

            if (result.Status == 200)
            {
                return Ok(result);
            }

            return StatusCode(
                result.Status,
                result
            );
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateOngoingCourseDto ongoingCourse)
        {
            try
            {
                if (id != ongoingCourse.Id)
                {
                    return BadRequest();
                }

                var result = await _updateOngoingCoursesService.CallAsync(
                    new UpdateOngoingCourseParameter(ongoingCourse)
                );

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<OngoingCourse>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var result = await _deleteOngoingCoursesService.CallAsync(new DeleteOngoingCourseParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (DbUpdateException)
            {
                return StatusCode(
                    422,
                    new Result<OngoingCourse>(
                        422,
                        null,
                        new List<string>() { "OngoingCourseas yra naudojamas kitur." }
                    )
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<OngoingCourse>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }
    }
}
