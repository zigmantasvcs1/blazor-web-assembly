﻿using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Dto.Lecturers;

namespace CoursesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturersController : ControllerBase
    {
        private readonly IService<ListLecturerParameter, List<LecturerDto>> _listLecturersService;
        private readonly IService<CreateLecturerParameter, LecturerDto> _createLecturersService;
        private readonly IService<UpdateLecturerParameter, LecturerDto> _updateLecturersService;
        private readonly IService<GetLecturerParameter, LecturerDto> _getLecturersService;
        private readonly IService<DeleteLecturerParameter, LecturerDto> _deleteLecturersService;

        public LecturersController(
            IService<ListLecturerParameter, List<LecturerDto>> listLecturersService,
            IService<CreateLecturerParameter, LecturerDto> createLecturersService,
            IService<UpdateLecturerParameter, LecturerDto> updateLecturersService,
            IService<GetLecturerParameter, LecturerDto> getLecturersService,
            IService<DeleteLecturerParameter, LecturerDto> deleteLecturersService)
        {
            _listLecturersService = listLecturersService;
            _createLecturersService = createLecturersService;
            _updateLecturersService = updateLecturersService;
            _getLecturersService = getLecturersService;
            _deleteLecturersService = deleteLecturersService;
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync()
        {
            try
            {
                var lecturers = await _listLecturersService.CallAsync(
                    new ListLecturerParameter(10)
                );

                return Ok(lecturers);
            }
            catch (Exception ex)
            {
                return StatusCode(
                    500,
                    new Result<Lecturer>(
                        500,
                        null,
                        new List<string>() { "Klaida" }
                    )
                );
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var result = await _getLecturersService.CallAsync(new GetLecturerParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Lecturer>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateLecturerDto lecturer)
        {
            var result = await _createLecturersService.CallAsync(
                new CreateLecturerParameter(lecturer)
            );

            if (result.Status == 200)
            {
                return Ok(result);
            }

            return StatusCode(
                result.Status,
                result
            );
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateLecturerDto lecturer)
        {
            try
            {
                if (id != lecturer.Id)
                {
                    return BadRequest();
                }

                var result = await _updateLecturersService.CallAsync(
                    new UpdateLecturerParameter(lecturer)
                );

                return Ok(result);
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Lecturer>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var result = await _deleteLecturersService.CallAsync(new DeleteLecturerParameter(id));

                if (result.Data == null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (DbUpdateException)
            {
                return StatusCode(
                    422,
                    new Result<Lecturer>(
                        422,
                        null,
                        new List<string>() { "Lectureras yra naudojamas kitur." }
                    )
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    500,
                    new Result<Lecturer>(
                        500,
                        null,
                        new List<string>() { exception.Message }
                    )
                );
            }
        }
    }
}
