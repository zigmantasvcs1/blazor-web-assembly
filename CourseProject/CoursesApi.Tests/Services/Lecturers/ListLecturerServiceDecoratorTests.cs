﻿using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Lecturers;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Shared.Dto.Lecturers;

namespace LecturersApi.Tests.Services.Lecturers
{
    [TestClass]
    public class ListLecturerServiceDecoratorTests
    {
        private ListLecturerParameter parameter = null!;
        private Mock<IService<ListLecturerParameter, List<LecturerDto>>> listLecturerServiceMock = null!;
        private Mock<ILogger<ListLecturerServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new ListLecturerParameter(2);
            listLecturerServiceMock = new Mock<IService<ListLecturerParameter, List<LecturerDto>>>();
            loggerMock = new Mock<ILogger<ListLecturerServiceDecorator>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            listLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<List<LecturerDto>>(200, new List<LecturerDto>()));

            // act
            await service.CallAsync(parameter);

            // assert
            listLecturerServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            listLecturerServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private ListLecturerServiceDecorator CreateService()
        {
            return new ListLecturerServiceDecorator(
                listLecturerServiceMock.Object,
                loggerMock.Object
            );
        }

        private Mock<ILogger<ListLecturerServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<ListLecturerServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
