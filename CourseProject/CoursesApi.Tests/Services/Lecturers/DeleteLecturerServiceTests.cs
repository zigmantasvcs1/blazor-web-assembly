﻿using CoursesApi.Models.Lecturers.Parameters;
using CoursesApi.Services.Courses;
using CoursesApi.Services.Lecturers;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Lecturers
{
    [TestClass]
    public class DeleteLecturerServiceTests
    {
        private DeleteLecturerParameter parameter = null!;
        private Mock<IRepository<Lecturer>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteLecturerParameter(1);
            lecturerRepositoryMock = new Mock<IRepository<Lecturer>>();
        }

        private DeleteLecturerService CreateService()
        {
            return new DeleteLecturerService(lecturerRepositoryMock.Object);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsSuccessResultWhenDeleteIsSuccessful()
        {
            // arrange
            lecturerRepositoryMock.Setup(repo => repo.DeleteAsync(It.IsAny<int>())).ReturnsAsync(true);
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public async Task ReturnsNotFoundResultWhenDeleteFails()
        {
            // arrange
            lecturerRepositoryMock
                .Setup(repo => repo.DeleteAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(404, result.Status);
            Assert.IsNull(result.Data);
        }

        [TestMethod]
        public async Task CallsLecturerRepositoryDeleteOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.DeleteAsync(parameter.Id), Times.Once);
        }
    }
}
