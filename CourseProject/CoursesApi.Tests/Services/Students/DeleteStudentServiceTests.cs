﻿using CoursesApi.Models.Students.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class DeleteStudentServiceTests
    {
        private DeleteStudentParameter parameter = null!;
        private Mock<IRepository<Student>> studentRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteStudentParameter(1);
            studentRepositoryMock = new Mock<IRepository<Student>>();
        }

        private DeleteStudentService CreateService()
        {
            return new DeleteStudentService(studentRepositoryMock.Object);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsSuccessResultWhenDeleteIsSuccessful()
        {
            // arrange
            studentRepositoryMock.Setup(repo => repo.DeleteAsync(It.IsAny<int>())).ReturnsAsync(true);
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public async Task ReturnsNotFoundResultWhenDeleteFails()
        {
            // arrange
            studentRepositoryMock
                .Setup(repo => repo.DeleteAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(404, result.Status);
            Assert.IsNull(result.Data);
        }

        [TestMethod]
        public async Task CallsStudentRepositoryDeleteOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            studentRepositoryMock.Verify(s => s.DeleteAsync(parameter.Id), Times.Once);
        }
    }
}
