﻿using CoursesApi.Models.Students.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Providers;
using CoursesApi.Services.Students;
using Shared.Dto.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class CreateStudentServiceTests
    {
        private CreateStudentParameter parameter = null!;

        private Student studentRepositoryResult = null!;
        private DateTime dateTimeNowProviderResult;

        private Mock<IRepository<Student>> studentRepositoryMock = null!;
        private Mock<IDateTimeNowProvider> dateTimeNowProviderMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateStudentParameter();

            dateTimeNowProviderResult = new DateTime(1999, 1, 1);

            studentRepositoryResult = CreateStudent();

            studentRepositoryMock = GetStudentRepositoryMock();
            dateTimeNowProviderMock = GetDateTimeNowProviderMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsStudentRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            studentRepositoryMock.Verify(
                s => s.CreateAsync(
                    It.Is<Student>(
                        fromMock => fromMock.Id == 0
                        && fromMock.Name == "test"
                        && fromMock.Surname == "testinukas"
                        && fromMock.Email == "testinukas@gmail.com"
                        && fromMock.DocumentNumber == "AAA001"
                        && fromMock.BirthDay == new DateTime(1999, 1, 1)
                        && fromMock.CreatedAt == dateTimeNowProviderResult
                    )
                )
            );

            studentRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromStudentRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            CollectionAssert.AreEqual(new List<string>(), result.Errors);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(111, result.Data.Id);
            Assert.AreEqual("test", result.Data.Name);
            Assert.AreEqual("testinukas", result.Data.Surname);
            Assert.AreEqual("testinukas@gmail.com", result.Data.Email);
            Assert.AreEqual("AAA001", result.Data.DocumentNumber);
            Assert.AreEqual(new DateTime(1999, 1, 1), result.Data.BirthDay);
        }

        private CreateStudentService CreateService()
        {
            return new CreateStudentService(
                studentRepositoryMock.Object,
                dateTimeNowProviderMock.Object
            );
        }

        private CreateStudentParameter CreateCreateStudentParameter()
        {
            return new CreateStudentParameter(
                new CreateStudentDto()
                {
                    Name = "test",
                    Surname = "testinukas",
                    Email = "testinukas@gmail.com",
                    DocumentNumber = "AAA001",
                    BirthDay = new DateTime(1999, 1, 1)
                }
            );
        }

        private Student CreateStudent()
        {
            return new Student()
            {
                Id = 111,
                Name = "test",
                Surname = "testinukas",
                Email = "testinukas@gmail.com",
                DocumentNumber = "AAA001",
                BirthDay = new DateTime(1999, 1, 1),
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Student>> GetStudentRepositoryMock()
        {
            var mock = new Mock<IRepository<Student>>();

            mock
                .Setup(
                    repo => repo.CreateAsync(It.IsAny<Student>())
                )
                .ReturnsAsync(studentRepositoryResult);

            return mock;
        }

        private Mock<IDateTimeNowProvider> GetDateTimeNowProviderMock()
        {
            var mock = new Mock<IDateTimeNowProvider>();

            mock
                .Setup(
                    provider => provider.Get()
                )
                .Returns(dateTimeNowProviderResult);

            return mock;
        }
    }
}
