﻿using CoursesApi.Models.Students.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.Students;
using Shared.Dto.Students;

namespace CoursesApi.Tests.Services.Students
{
    [TestClass]
    public class UpdateStudentServiceDecoratorTests
    {
        private UpdateStudentParameter parameter = null!;
        private Mock<IService<UpdateStudentParameter, StudentDto>> updateStudentServiceMock = null!;
        private Mock<ILogger<UpdateStudentServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateUpdateStudentParameter();

            updateStudentServiceMock = new Mock<IService<UpdateStudentParameter, StudentDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            updateStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<StudentDto>(200, new StudentDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            updateStudentServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            updateStudentServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private UpdateStudentServiceDecorator CreateService()
        {
            return new UpdateStudentServiceDecorator(
                updateStudentServiceMock.Object,
                loggerMock.Object
            );
        }

        private UpdateStudentParameter CreateUpdateStudentParameter()
        {
            return new UpdateStudentParameter(new UpdateStudentDto());
        }

        private Mock<ILogger<UpdateStudentServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<UpdateStudentServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
