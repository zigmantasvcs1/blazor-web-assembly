﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Courses;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Shared.Dto.Courses;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class DeleteCourseServiceDecoratorTests
    {
        private DeleteCourseParameter parameter = null!;
        private Mock<IService<DeleteCourseParameter, CourseDto>> deleteCourseServiceMock = null!;
        private Mock<ILogger<DeleteCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = new DeleteCourseParameter(1); // Assuming this is how you create your parameter
            deleteCourseServiceMock = new Mock<IService<DeleteCourseParameter, CourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            deleteCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<CourseDto>(200, new CourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            deleteCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            deleteCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private DeleteCourseServiceDecorator CreateService()
        {
            return new DeleteCourseServiceDecorator(
                deleteCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private Mock<ILogger<DeleteCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<DeleteCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
