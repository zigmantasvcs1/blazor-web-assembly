﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Services.Courses;
using DataAccess;
using DataAccess.Entities;
using Moq;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class GetCourseServiceTests
    {
        private GetCourseParameter parameter = null!;
        private Course lecturerRepositoryResult = null!;

        private Mock<IRepository<Course>> lecturerRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetCourseParameter();

            lecturerRepositoryResult = CreateCourse();

            lecturerRepositoryMock = GetCourseRepositoryMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsCourseRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(s => s.GetAsync(parameter.Id), Times.Once);
            lecturerRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromCourseRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(lecturerRepositoryResult.Id, result.Data.Id);
            Assert.AreEqual(lecturerRepositoryResult.Title, result.Data.Title);
            Assert.AreEqual(lecturerRepositoryResult.Description, result.Data.Description);
            Assert.AreEqual(lecturerRepositoryResult.Hours, result.Data.Hours);
            Assert.AreEqual(lecturerRepositoryResult.Price, result.Data.Price);
        }

        private GetCourseService CreateService()
        {
            return new GetCourseService(lecturerRepositoryMock.Object);
        }

        private GetCourseParameter CreateGetCourseParameter()
        {
            return new GetCourseParameter(1);
        }

        private Course CreateCourse()
        {
            return new Course()
            {
                Id = 111,
                Title = "test",
                Description = "testinukas",
                Hours = 10,
                Price = 20,
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Course>> GetCourseRepositoryMock()
        {
            var mock = new Mock<IRepository<Course>>();

            mock.Setup(repo => repo.GetAsync(It.IsAny<int>())).ReturnsAsync(lecturerRepositoryResult);

            return mock;
        }
    }
}
