﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services.Courses;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Shared.Dto.Courses;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class GetCourseServiceDecoratorTests
    {
        private GetCourseParameter parameter = null!;
        private Mock<IService<GetCourseParameter, CourseDto>> getCourseServiceMock = null!;
        private Mock<ILogger<GetCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateGetCourseParameter();

            getCourseServiceMock = new Mock<IService<GetCourseParameter, CourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            getCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<CourseDto>(200, new CourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            getCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            getCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private GetCourseServiceDecorator CreateService()
        {
            return new GetCourseServiceDecorator(
                getCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private GetCourseParameter CreateGetCourseParameter()
        {
            return new GetCourseParameter(1);
        }

        private Mock<ILogger<GetCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<GetCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
