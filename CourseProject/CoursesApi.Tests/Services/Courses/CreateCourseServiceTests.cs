﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Providers;
using CoursesApi.Services.Courses;
using DataAccess;
using DataAccess.Entities;
using Moq;
using Shared.Dto.Courses;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class CreateCourseServiceTests
    {
        private CreateCourseParameter parameter = null!;

        private Course lecturerRepositoryResult = null!;
        private DateTime dateTimeNowProviderResult;

        private Mock<IRepository<Course>> lecturerRepositoryMock = null!;
        private Mock<IDateTimeNowProvider> dateTimeNowProviderMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateCreateCourseParameter();

            dateTimeNowProviderResult = new DateTime(1999, 1, 1);

            lecturerRepositoryResult = CreateCourse();

            lecturerRepositoryMock = GetCourseRepositoryMock();
            dateTimeNowProviderMock = GetDateTimeNowProviderMock();
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await service.CallAsync(null));
        }

        [TestMethod]
        public async Task CallsCourseRepositoryOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            lecturerRepositoryMock.Verify(
                s => s.CreateAsync(
                    It.Is<Course>(
                        fromMock => fromMock.Id == 0
                        && fromMock.Title == "test"
                        && fromMock.Description == "testinukas"
                        && fromMock.Hours == 10
                        && fromMock.Price == 20
                        && fromMock.CreatedAt == dateTimeNowProviderResult
                    )
                )
            );

            lecturerRepositoryMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task ResultOfServiceContainsValuesFromCourseRepository()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            CollectionAssert.AreEqual(new List<string>(), result.Errors);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(111, result.Data.Id);
            Assert.AreEqual("test", result.Data.Title);
            Assert.AreEqual("testinukas", result.Data.Description);
            Assert.AreEqual(10, result.Data.Hours);
            Assert.AreEqual(20, result.Data.Price);
        }

        private CreateCourseService CreateService()
        {
            return new CreateCourseService(
                lecturerRepositoryMock.Object,
                dateTimeNowProviderMock.Object
            );
        }

        private CreateCourseParameter CreateCreateCourseParameter()
        {
            return new CreateCourseParameter(
                new CreateCourseDto()
                {
                    Title = "test",
                    Description = "testinukas",
                    Hours = 10,
                    Price = 20
                }
            );
        }

        private Course CreateCourse()
        {
            return new Course()
            {
                Id = 111,
                Title = "test",
                Description = "testinukas",
                Hours = 10,
                Price = 20,
                CreatedAt = new DateTime(2023, 12, 10)
            };
        }

        private Mock<IRepository<Course>> GetCourseRepositoryMock()
        {
            var mock = new Mock<IRepository<Course>>();

            mock
                .Setup(
                    repo => repo.CreateAsync(It.IsAny<Course>())
                )
                .ReturnsAsync(lecturerRepositoryResult);

            return mock;
        }

        private Mock<IDateTimeNowProvider> GetDateTimeNowProviderMock()
        {
            var mock = new Mock<IDateTimeNowProvider>();

            mock
                .Setup(
                    provider => provider.Get()
                )
                .Returns(dateTimeNowProviderResult);

            return mock;
        }
    }
}
