﻿using CoursesApi.Models.Courses.Parameters;
using CoursesApi.Models;
using CoursesApi.Services;
using Microsoft.Extensions.Logging;
using Moq;
using CoursesApi.Services.Courses;
using Shared.Dto.Courses;

namespace CoursesApi.Tests.Services.Courses
{
    [TestClass]
    public class UpdateCourseServiceDecoratorTests
    {
        private UpdateCourseParameter parameter = null!;
        private Mock<IService<UpdateCourseParameter, CourseDto>> updateCourseServiceMock = null!;
        private Mock<ILogger<UpdateCourseServiceDecorator>> loggerMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            parameter = CreateUpdateCourseParameter();

            updateCourseServiceMock = new Mock<IService<UpdateCourseParameter, CourseDto>>();
            loggerMock = GetLoggerMock();
        }

        [TestMethod]
        public async Task CallAsyncLogsInformationAtStartAndEnd()
        {
            // arrange
            var service = CreateService();

            // act
            await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.AtLeast(2)
            );
        }

        [TestMethod]
        public async Task CallAsyncForwardsCallToDecoratedService()
        {
            // arrange
            var service = CreateService();
            updateCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ReturnsAsync(new Result<CourseDto>(200, new CourseDto()));

            // act
            await service.CallAsync(parameter);

            // assert
            updateCourseServiceMock.Verify(s => s.CallAsync(parameter), Times.Once);
        }

        [TestMethod]
        public async Task CallAsyncLogsErrorAndReturns500WhenExceptionOccurs()
        {
            // arrange
            var service = CreateService();

            updateCourseServiceMock
                .Setup(s => s.CallAsync(parameter))
                .ThrowsAsync(new Exception("Test exception"));

            // act
            var result = await service.CallAsync(parameter);

            // assert
            loggerMock.Verify(
                log => log.Log(
                    LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => true),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception, string>>()
                ),
                Times.Once
            );

            Assert.AreEqual(500, result.Status);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual("Kreipkites i adminsitratoriu", result.Errors[0]);
        }

        private UpdateCourseServiceDecorator CreateService()
        {
            return new UpdateCourseServiceDecorator(
                updateCourseServiceMock.Object,
                loggerMock.Object
            );
        }

        private UpdateCourseParameter CreateUpdateCourseParameter()
        {
            return new UpdateCourseParameter(new UpdateCourseDto());
        }

        private Mock<ILogger<UpdateCourseServiceDecorator>> GetLoggerMock()
        {
            var mock = new Mock<ILogger<UpdateCourseServiceDecorator>>();

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Information,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
            );

            mock
                .Setup(
                    x => x.Log(
                        LogLevel.Error,
                        It.IsAny<EventId>(),
                        It.Is<It.IsAnyType>((v, t) => true),
                        It.IsAny<Exception>(),
                        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                );

            return mock;
        }
    }
}
