﻿using CoursesApi.Models.OngoingCourses.Parameters;
using DataAccess.Entities;
using DataAccess;
using Moq;
using CoursesApi.Services.OngoingCourses;
using Shared.Dto.OngoingCourses;

namespace CoursesApi.Tests.Services.OngoingCourses
{
    [TestClass]
    public class UpdateOngoingCourseServiceTests
    {
        private UpdateOngoingCourseParameter parameter = null!;
        private OngoingCourse ongoingCourseRepositoryUpdateResult = null!;
        private OngoingCourse ongoingCourseRepositoryGetResult = null!;
        private Mock<IRepository<OngoingCourse>> ongoingCourseRepositoryMock = null!;

        [TestInitialize]
        public void TestInitialize()
        {
            var updateOngoingCourseDto = new UpdateOngoingCourseDto
            {
                // Initialize with test data
            };
            parameter = new UpdateOngoingCourseParameter(updateOngoingCourseDto);
            ongoingCourseRepositoryUpdateResult = CreateOngoingCourse(new DateTime(2000, 1, 1));
            ongoingCourseRepositoryGetResult = CreateOngoingCourse(new DateTime(2000, 1, 2));
            ongoingCourseRepositoryMock = new Mock<IRepository<OngoingCourse>>();

            ongoingCourseRepositoryMock
                .Setup(ongoingCourseRepository => ongoingCourseRepository.GetAsync(It.IsAny<int>()))
                .ReturnsAsync(ongoingCourseRepositoryGetResult);

            ongoingCourseRepositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<OngoingCourse>()))
                .ReturnsAsync(ongoingCourseRepositoryUpdateResult);
        }

        [TestMethod]
        public async Task ThrowsArgumentNullExceptionWhenParameterIsNull()
        {
            // arrange
            var service = CreateService();

            // act & assert
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => service.CallAsync(null));
        }

        [TestMethod]
        public async Task ReturnsUpdatedOngoingCourseDto()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            Assert.AreEqual(200, result.Status);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(ongoingCourseRepositoryUpdateResult.Id, result.Data.Id);
            Assert.AreEqual(ongoingCourseRepositoryUpdateResult.StartDate, result.Data.StartDate);
        }

        [TestMethod]
        public async Task CallsOngoingCourseRepositoryUpdateOnce()
        {
            // arrange
            var service = CreateService();

            // act
            var result = await service.CallAsync(parameter);

            // assert
            ongoingCourseRepositoryMock.Verify(s => s.UpdateAsync(It.IsAny<OngoingCourse>()), Times.Once);
        }

        private UpdateOngoingCourseService CreateService()
        {
            return new UpdateOngoingCourseService(ongoingCourseRepositoryMock.Object);
        }

        private OngoingCourse CreateOngoingCourse(DateTime startDate)
        {
            return new OngoingCourse()
            {
                StartDate = startDate
            };
        }
    }
}
