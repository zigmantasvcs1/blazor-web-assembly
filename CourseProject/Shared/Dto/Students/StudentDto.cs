﻿namespace Shared.Dto.Students
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public int? Age { get; set; }
    }
}
