﻿namespace Shared.Dto.OngoingCourses
{
    public class UpdateOngoingCourseDto
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
