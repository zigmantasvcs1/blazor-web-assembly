﻿namespace Shared.Dto.OngoingCourses
{
    public class CreateOngoingCourseDto
    {
        public int CourseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
