﻿namespace Shared.Dto.Courses
{
    public class CreateCourseDto
    {
        public decimal Price { get; set; }
        public string Title { get; set; }
        public int Hours { get; set; }
        public string Description { get; set; }
    }
}
