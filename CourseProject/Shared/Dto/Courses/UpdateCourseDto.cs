﻿namespace Shared.Dto.Courses
{
    public class UpdateCourseDto
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }
        public int Hours { get; set; }
        public string Description { get; set; }
    }
}
