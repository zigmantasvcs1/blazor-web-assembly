﻿using DataAccess.Entities;

namespace DataAccess
{
    public class CoursesRepository : BaseRepository<Course>
    {
        public CoursesRepository(CoursesDbContext context) : base(context)
        {
        }
    }
}
