﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class OngoingCourse : BaseEntity
    {
        [Required]
        public int CourseId { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }
    }
}
