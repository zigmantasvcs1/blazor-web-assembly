﻿namespace DataAccess.Entities
{
    public class Student : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? BirthDay { get; set; }

        public override string ToString()
        {
            return $"{Id}/{Name}/{Surname}/{Email}";
        }
    }
}
