﻿using DataAccess.Entities;

namespace DataAccess
{
    public class LecturersRepository : BaseRepository<Lecturer>
    {
        public LecturersRepository(CoursesDbContext context) : base(context)
        {
        }
    }
}
